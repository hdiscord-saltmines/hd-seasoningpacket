mixin class UaS_BigMREPouch {
	default {
		UaS_Consumable.Calories 800;
		UaS_Consumable.Fluid 400;
		UaS_Consumable.Bulk 18;
		+UaS_Consumable.PACKAGED;
		+UaS_Consumable.RESEALABLE;
		Inventory.PickupMessage "Picked up a large vacuum-sealed MRE pouch.";
	}
}

mixin class UaS_SmallMREItem {
	default {
		UaS_Consumable.Calories 400;
		UaS_Consumable.Fluid 200;
		UaS_Consumable.Bulk 12;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a small vacuum-sealed MRE item.";
		scale 0.65;
	}
}

mixin class UaS_SingleSnack {
	default {
		UaS_Consumable.Calories 200;
		UaS_Consumable.Fluid 0;
		UaS_Consumable.Bulk 5;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a packaged snack.";
		scale 0.40;
	}
}

mixin class UaS_CanSodaMixin { //Canned Drinks (all soda cal/ml roughly based off cola)
	default {
		UaS_Consumable.Calories 120;
		UaS_Consumable.Fluid 350;
		UaS_Consumable.Bulk 6;
		UaS_Consumable.OpenSound "mre/canopen";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a can of soda.";
		Inventory.Icon "SP01I0";
		scale 0.3;
	}
}

mixin class UaS_BottleSodaMixin { //Bottled Drinks
	default {
		UaS_Consumable.Calories 240;
		UaS_Consumable.Fluid 600;
		UaS_Consumable.Bulk 8;
		UaS_Consumable.OpenSound "mre/sodabottle";
		UaS_Consumable.CloseSound "mre/sodalid";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		+UaS_Consumable.RESEALABLE;
		Inventory.PickupMessage "Picked up a bottle of soda.";
		Inventory.Icon "SP01H0";
		scale 0.3;
	}
}

mixin class UaS_LargeSodaMixin { //2L Soda Bottles
	default {
		UaS_Consumable.Calories 720;
		UaS_Consumable.Fluid 2000;
		UaS_Consumable.Bulk 20;
		UaS_Consumable.OpenSound "mre/sodabottle";
		UaS_Consumable.CloseSound "mre/sodalid";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		+UaS_Consumable.RESEALABLE;
		Inventory.PickupMessage "Picked up a large bottle of soda.";
		Inventory.Icon "SP01H0";
		scale 0.45;
	}
}

mixin class UaS_CoffeePack {
	default {
		UaS_Consumable.Calories 150;
		UaS_Consumable.Fluid 250;
		UaS_Consumable.Bulk 5;
        UaS_Consumable.Description "";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a disposable, self-heating water pouch with a pack of coffee mix.";
		Inventory.Icon "HSCVR0";
		scale 0.45;
	}
}

mixin class UaS_Milkpouch{
	default {
		UaS_Consumable.Calories 250;
		UaS_Consumable.Fluid 250;
		UaS_Consumable.Bulk 5;
        UaS_Consumable.Description "A pouch of pasturized, shelf-stable whole milk. Includes cooling pouch.";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a box of milk!";
		Inventory.Icon "MLBTR0";
		scale 0.45;
	}
	states(actor){
	spawn:
		MLBT R -1;
		stop;
	}
}

mixin class UaS_DrinkPack {
	default {
		UaS_Consumable.Calories 100;
		UaS_Consumable.Fluid 300;
		UaS_Consumable.Bulk 8;
        UaS_Consumable.Description "";
		+UaS_Consumable.DRINKABLE;
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a disposable drink pouch. Better take a closer look...";
		Inventory.Icon "HSCVR0";
		scale 0.45;
	}
}

mixin class UaS_BulkBox { //for containers of food
	default {
		UaS_Consumable.Calories 1;
		UaS_Consumable.Fluid 1;
		UaS_Consumable.Bulk 16;
        UaS_Consumable.Description "";
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a box of something. Better take a closer look...";
		Inventory.Icon "HSCVR0";
		scale 0.45;
	}
}

mixin class UaS_PunySnack { //for da beans
	default {
		UaS_Consumable.Calories 32;
		UaS_Consumable.Fluid 10;
		UaS_Consumable.Bulk 1;
		+UaS_Consumable.NOSPAWN; // Exclude from random spawns
        UaS_Consumable.Description "";
		Inventory.PickupMessage "Picked up something tiny. Better take a closer look...";
		Inventory.Icon "HSCVR0";
		scale 0.1;
	}
}

mixin class UaS_CandyBar{
	default {
		UaS_Consumable.Calories 800;
		UaS_Consumable.Fluid 160;
		UaS_Consumable.Bulk 6;
        UaS_Consumable.Description "A candy bar. A single ingot of sweet deliciousness.";
		+UaS_Consumable.PACKAGED;
		Inventory.PickupMessage "Picked up a candy bar!";
		Inventory.Icon "SP01L0";
		scale 0.2;
	}
	states(actor){
	spawn:
		SP01 L -1;
		stop;
	}
}